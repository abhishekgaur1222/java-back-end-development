package com.sapient.et.calc;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class calculatorTest {
	
	Calculator calc;
	
	
	@BeforeClass
	public  void setCalculatorRef(){
		calc = new CalculatorImpl(); 
	}
	
	@Test
	public void whenSubtractTwoNumbersItShouldReturnSubtractedValue(){
		
		int val = calc.subtract(7, 5);
		Assert.assertEquals(val, 12);
	}
	
	@Test
	public void whenAddTwoNumberItShouldReturnSum(){		
		int result = calc.add(10, 7);	
		Assert.assertEquals(2, result);
	}

}
