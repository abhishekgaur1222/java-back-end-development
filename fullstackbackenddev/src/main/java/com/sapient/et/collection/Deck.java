package com.sapient.et.collection;
import java.util.*;

class Deck {
    private ArrayList<Card> deck;
//    private Map<Card, String> suitDeck;
    private ArrayList<Card> suitDeck;
    private String[] ranks ={"ACE","2","3","4","5","6","7","8","9","10","JACK","QUEEN","KING"};
    private String[] suits ={"SPADE","HEART","CLUB","DIAMOND"};

    public Deck() {
        deck = new ArrayList<Card>();
        for(int i=0;i<suits.length;i++) {
            for(int j=0;j<ranks.length;j++) {
                deck.add(new Card(ranks[j],suits[i]));
            }
        }
    }
    
    
    public ArrayList<Card> PrepareDeck(String suits) {
    	deck = new ArrayList<Card>();
    	for(int j=0;j<ranks.length;j++) {
        	deck.add(new Card(ranks[j],suits));
        }
    	return deck;
    }
    
    public Deck(String suits) {
    	deck = new ArrayList<Card>();
    	for(int j=0;j<ranks.length;j++) {
        	suitDeck.add(new Card(ranks[j],suits));
        }
    }
    
    public void showCards() {
        System.out.println("\n\n Showing Cards !!!");
        for(Card card: deck){
            System.out.println("Card: "+card.getRank()+" Suit: "+card.getSuit());
        }
    }

    public void showCards(ArrayList<Card> lst) {
        System.out.println("\n\n Showing Cards !!!");
        int i=1;
        for(Card c:lst) {
            System.out.println("Card "+(i++)+" : "+c);
        }
    }

    public  void shuffle() {
        ArrayList<Card> temp = new ArrayList<Card>();
        while(!deck.isEmpty()) {
            int loc=(int)(Math.random()*deck.size());
            temp.add(deck.get(loc));
            deck.remove(loc);   
        }
        deck=temp;
    }       
    
    public ArrayList<Card> shuffle(ArrayList<Card> lst) {
        ArrayList<Card> temp = new ArrayList<Card>();
        while(!lst.isEmpty()) {
            int loc=(int)(Math.random()*lst.size());
            temp.add(lst.get(loc));
            lst.remove(loc);   
        }
        lst=temp;
        return lst;
    }
    
//    public  void shuffle(String cardSuit) {
//        ArrayList<Card> temp = new ArrayList<Card>();
//        while(!deck.isEmpty()) {
//            int loc=(int)(Math.random()*deck.size());
//            temp.add(deck.get(loc));
//            deck.remove(loc);   
//        }
//        deck=temp;
//    }

}