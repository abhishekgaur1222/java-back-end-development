package com.sapient.et.collection;

import java.util.ArrayList;

public class Game {
    public static void main(String[] args) {
        Deck myDeck = new Deck();
        
        myDeck.showCards();
        
        ArrayList<Card> al = new ArrayList<Card>();
        
        al = myDeck.PrepareDeck("SPADE");
        
        System.out.println("After Shuffling");
        al = myDeck.shuffle(al);
        
        
        myDeck.showCards(al);
//        myDeck.shuffle();
//        System.out.println("-------After Shuffling----");
//        myDeck.showCards();
//        
//        System.out.println("-------After shuffling a suit of cards---------");
//        Deck myDeckShuffle = new Deck("SPADE"); 
//        myDeckShuffle.shuffle();
//        myDeck.showCards();
    }

}
