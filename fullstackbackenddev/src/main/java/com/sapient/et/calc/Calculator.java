package com.sapient.et.calc;

public interface Calculator {

	public int add(int a, int b);
	public int subtract(int a, int b);
}
