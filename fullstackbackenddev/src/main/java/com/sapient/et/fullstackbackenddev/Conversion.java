package com.sapient.et.fullstackbackenddev;

/**
 * Hello world!
 *
 */
public class Conversion 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
    
    public double tempConversion(double temperature, String unit){
    	if(unit.equals("F")){
    		return (temperature - 32) * (5.0/9.0);
    	}
    	else
    		return (temperature * (9.0/5.0)) + 32;
    }
    
}
